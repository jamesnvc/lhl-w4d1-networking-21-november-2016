//
//  MasterViewController.h
//  NetworkDemo
//
//  Created by James Cash on 21-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

