//
//  GithubRepo.m
//  NetworkDemo
//
//  Created by James Cash on 21-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "GithubRepo.h"

@implementation GithubRepo

- (instancetype)initWithName:(NSString*)name andURL:(NSURL*)url
{
    self = [super init];
    if (self) {
        _name = name;
        _url = url;
    }
    return self;
}

@end
