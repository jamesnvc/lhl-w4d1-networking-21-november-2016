//
//  GithubRepo.h
//  NetworkDemo
//
//  Created by James Cash on 21-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GithubRepo : NSObject

@property (nonatomic,strong) NSURL *url;
@property (nonatomic,strong) NSString *name;

- (instancetype)initWithName:(NSString*)name andURL:(NSURL*)url;

@end
