//
//  DetailViewController.h
//  NetworkDemo
//
//  Created by James Cash on 21-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDate *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

